
# Дипломная работа по курсу "DevOps-инженер"

## Этапы выполнения:

### 1. Регистрация доменного имени.

Зарегистрировано доменное имя `domloro.xyz`. Домен делигирован в Yandex Cloud, в настройках указаны соответствующие имена NameServers:

![Domain registrator](images/domain.png)

### 2. Подготовка инфраструктуры.

  1. Предварительно с помощью cli-утилиты создан сервисный аккаунт для авторизации в облаке. Для аккаунта получен и сохранен в файл ключ авторизаци в облаке и статический для доступа к S3-бекэнду.
    Необходимо задать переменные окружения c чувствительными данными:
  
  ```bash
    $ export TF_VAR_YC_SECRET_KEY=YCO***************************
    $ export TF_VAR_YC_ACCESS_KEY=YCA******************
    $ export TF_VAR_YC_CLOUD_ID=b1gs*****************
    $ export TF_VAR_YC_FOLDER_ID=b1g8****************
  ```
  2. S3 yandex cloud bucket инициализируется и создается в отдельной папке с помощью Terraform.

  ![S3 Bucket](images/bucket.png)

  3. Создан workspace с названием stage.

```bash
$ terraform workspace list
  default
* stage
```

Для инициализации backend необходимо перейти в рабочий каталог terraform и запустить команду: 

```bash
  $ terraform init -backend-config="access_key=$TF_VAR_YC_ACCESS_KEY" \
-backend-config="secret_key=$TF_VAR_YC_SECRET_KEY"
```

Состояние проекта в этом рабочем пространстве сохраняется в созданном бакете:

![Backend](images/backend.png)

4. В отдельном файле terraform `network.tf` создаются VPS с подсетями в разных зонах доступности. Из файла `dns.tf` создаются DNS записи ресурсов в cloud yandex DNS для внешнего адреса инфраструктуры. Для выхода наружу серверов приватной зоны создается статический маршрут из файла `static_route.tf`.

5. Одной командой `terraform apply` создаются необходимые облачные ресурсы инфраструктуры:

![Cloud Resources](images/cloud-resources.png)

Виртуальные хосты для сервисов разворачиваются на базе образа Ubuntu:

![Cloud Compute](images/cloud-compute.png)

Командой `terraform destroy` все созданные ресурсы уничтожаются.

---

### 3. Установка Nginx и LetsEncrypt.

1. Для установки и настройки Nginx и LetsEncrypt разработаны роли:
  
  * [install-nginx](src/ansible/roles/install-nginx/) - роль устанавливает nginx;
  * [enable-nat](src/ansible/roles/enable-nat/) - включает форвардинг и настраивает NAT;
  * [install-certbot](src/ansible/roles/install-certbot/) - устанавливает необходимые компоненты для генерации сертификатов;
  * [start-certbot](src/ansible/roles/start-certbot/) - создание сертификатов сервисов.

В результате работы плейбуков и выполнения ролей на виртуальной машине устанавливается и настраивается reverce-proxy nginx сервер. Установленной утилитой LetsEncript certbot получаем тестовые сертификаты сервисов:   

![Nginx Certs](images/nginx-cert.png)

```bash
null_resource.certbot (local-exec): TASK [Test certificates] *******************************************************
null_resource.certbot (local-exec): changed: [node01_nginx]

null_resource.certbot (local-exec): TASK [debug] *******************************************************************
null_resource.certbot (local-exec): ok: [node01_nginx] => {
null_resource.certbot (local-exec):     "msg": [
null_resource.certbot (local-exec):         "alertmanager.domloro.xyz",
null_resource.certbot (local-exec):         "domloro.xyz",
null_resource.certbot (local-exec):         "gitlab.domloro.xyz",
null_resource.certbot (local-exec):         "grafana.domloro.xyz",
null_resource.certbot (local-exec):         "prometheus.domloro.xyz",
null_resource.certbot (local-exec):         "www.domloro.xyz"
null_resource.certbot (local-exec):     ]
null_resource.certbot (local-exec): }
```
___
### 4. Установка кластера MySQL.

Для установки кластера MySQL разработаны роли:

  * [install-mysql](src/ansible/roles/install-mysql/) - роль установки сервера MySQL и cli-клиента;
  * [add-mysql-db](src/ansible/roles/add-mysql-db/) - роль автоматически создает базу данных для Wordpress и пользователя с полными правами для этой базы;
  * [replica-mysql](src/ansible/roles/replica-mysql/) - настройка кластера MySQL в режиме Master-Slave.

Результат работы ролей:

* db01

```bash
mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| sys                |
| wordpress          |
+--------------------+
5 rows in set (0.01 sec)

mysql> show grants for wordpress;
+----------------------------------------------------------------------------+
| Grants for wordpress@%                                                     |
+----------------------------------------------------------------------------+
| GRANT USAGE ON *.* TO `wordpress`@`%`                                      |
| GRANT ALL PRIVILEGES ON `wordpress`.* TO `wordpress`@`%` WITH GRANT OPTION |
+----------------------------------------------------------------------------+
2 rows in set (0.00 sec)

mysql> SHOW MASTER STATUS;
+------------------+----------+--------------+------------------+-------------------+
| File             | Position | Binlog_Do_DB | Binlog_Ignore_DB | Executed_Gtid_Set |
+------------------+----------+--------------+------------------+-------------------+
| mysql-bin.000001 |      157 | wordpress    |                  |                   |
+------------------+----------+--------------+------------------+-------------------+
1 row in set (0.00 sec)
```

* db02

```bash
mysql> SHOW REPLICA STATUS\G;
*************************** 1. row ***************************
             Replica_IO_State: Waiting for source to send event
                  Source_Host: 192.168.101.18
                  Source_User: replication
                  Source_Port: 3306
                Connect_Retry: 60
              Source_Log_File: mysql-bin.000001
          Read_Source_Log_Pos: 157
               Relay_Log_File: db02-relay-bin.000002
                Relay_Log_Pos: 373
        Relay_Source_Log_File: mysql-bin.000001
           Replica_IO_Running: Yes
          Replica_SQL_Running: Yes
              
```
___
### 5. Установка WordPress.

Для установки WordPress использованы роли:

  * [install-nginx](src/ansible/roles/install-nginx/) - установка nginx;
  * [install-php](src/ansible/roles/install-php/) - роль установки PHP;
  * [wordpress](src/ansible/roles/wordpress/) - ролью устанавливаются файлы сайта и подключение к базе данных MySQL;  
  * [wordpress-cli](src/ansible/roles/wordpress-cli/) - установка и конфиругация wordpress, задается пароль администартора.

В результате работы ролей, на виртуальную машину устанавливается Nginx и Wordpress. Сайт подключен к базе данных в кластере MySQL. 

![Wordpress](images/wordpress.png)

![Wordpress Admin](images/wordpress-admin.png)

---
### 6. Установка Gitlab CE и Gitlab Runner.

Созданы роли установки Gitlab И Gitlsb Runner:

  * [gitlab](src/ansible/roles/gitlab/) - роль установки сервера Gitlab CE, первичной настройки пароля авторизации пользователя root, получения токана для регистрации runner;
  * [gitlab-runner](src/ansible/roles/gitlab-runner/) - роль для установки gitlab runner и регистрации на сервере gitlab полученным токеном. Так же на виртуальном сервере генерируется ssh ключ, публичная часть которого регистрируется на сервере с WordPress. 

Результат работы ролей:

![Gitlab Runner](images/gitlab-runner.png)

Демонстрация работы автоматизарованного сi/cd для деплоя при создании нового тега:

Для нового пользователя `testusr` создаем публичный проект `testproject` в котором будет размещаться файл `test.html`, который будет передаваться в каталог сайта на сервере с WordPress.

![Test file](images/testfile.png)

Следующий пайплайн описывает деплой проекта в Wordpress при добавлении тега:

![Pireline](images/pipeline.png)

![Jobs](images/jobs.png)

Протокол работы доставки кода:

![Job result](images/job-result.png)

Проверка результата:

![My Site](images/testsite.png)

___
### 7. Установка Prometheus, Alert Manager, Node Exporter и Grafana.

  * [install-node-exporter](src/ansible/roles/install-node-exporter/) - роль устанавливает и запускает сервис на всех виртуальных машинах;
  * [install-alertmanager](src/ansible/roles/install-alertmanager/) - роль для установки Alertmanager и настройки правил для создания алертов;
  * [install-prometheus](src/ansible/roles/install-prometheus/) - установка и настройка Prometheus. Создаетася файл настроек получения метрик, добавляются правила алертов.
  * [install-grafana](src/ansible/roles/install-grafana/) - роль установки Grafana, настройки подключения к источнику данных Prometheus и установки дашборда для отображения основной информации о нодах.

Результат работы:

* Prometheus

![Prometheus Targets](images/teargets.png)

![Prometheus Alerst](images/alerts.png)

* Alertmanager

![Alertmanager](images/alertmanager.png)

* Grafana

![Grafana Dashboard](images/dashboard.png)

### 8. Выключение и удаление всей инфраструктуры.

```bash
  $ terraform destroy --auto-approve
```

![Destroy All](images/destroy.png)

---
