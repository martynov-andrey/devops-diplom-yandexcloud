resource "local_file" "inventory" {
  content  = <<-DOC
    ---
    # Ansible inventory containing variable values from Terraform.
    # Generated by Terraform.
    nginx:
      hosts:
        node01_nginx:
          ansible_host: ${yandex_compute_instance.node01-nginx.network_interface.0.nat_ip_address}
    
    dbsrvs:
      children:
        master:
        slave:
    
    master:
      vars:
        mysql_replication_role: "master"
        hostname: "db01"
      hosts:
        node02_db01:
          ansible_host: ${yandex_compute_instance.node02-db01.network_interface.0.ip_address}
    
    slave:
      vars:
        mysql_replication_role: "slave"
        hostname: "db02"
      hosts:
        node03_db02:
          ansible_host: ${yandex_compute_instance.node03-db02.network_interface.0.ip_address}
    
    appsrv:
      vars:
        hostname: "app"
        db_host: ${yandex_compute_instance.node02-db01.network_interface.0.ip_address}
      hosts:
        node04_appsrv:
          ansible_host: ${yandex_compute_instance.node04-appsrv.network_interface.0.ip_address}

    gitlab:
      hosts:
        node05_gitlab:
          ansible_host: ${yandex_compute_instance.node05-gitlab.network_interface.0.ip_address}

    runner:
      hosts:
        node06_runner:
          ansible_host: ${yandex_compute_instance.node06-runner.network_interface.0.ip_address}

    prometheus:
      hosts:
        node-07_monitoring:
          ansible_host: ${yandex_compute_instance.node07-monitoring.network_interface.0.ip_address}
    
    jumped:
      vars:
        ansible_ssh_common_args: '-o ProxyCommand="ssh -W %h:%p -q ubuntu@${yandex_compute_instance.node01-nginx.network_interface.0.nat_ip_address}"'
      children:
        dbsrvs:
        appsrv:
        gitlab:
        runner:
        prometheus:
    DOC
  filename = "../ansible/inventory"

  depends_on = [
    yandex_compute_instance.node01-nginx,
    yandex_compute_instance.node02-db01,
    yandex_compute_instance.node03-db02,
    yandex_compute_instance.node04-appsrv,
    yandex_compute_instance.node05-gitlab,
    yandex_compute_instance.node06-runner,
    yandex_compute_instance.node07-monitoring
  ]
}