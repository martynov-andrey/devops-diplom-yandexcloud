resource "yandex_compute_instance" "node02-db01" {
  name     = "node02-db01"
  zone     = "${var.YC_ZONE2}"
  hostname = "db01.${var.DOMAIN_NAME}"
  resources {
    core_fraction = "${var.CORE_FRACTION}"
    cores         = 4
    memory        = 4
  }
  scheduling_policy {
    preemptible = "${var.PREEMPTIBLE}"
  }
  boot_disk {
    initialize_params {
      image_id = "${var.IMAGE_ID}"
      name     = "root-node02-db01"
      type     = "network-nvme"
      size     = "15"
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet2.id
    nat       = false
  }
  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }

  depends_on = [
    yandex_vpc_subnet.subnet2
  ]
}
