resource "yandex_vpc_route_table" "route-nat" {
  name         = "route-table-nat"
  description  = "Route table for private subnet"

  depends_on = [
    yandex_compute_instance.node01-nginx
  ]
  network_id   = yandex_vpc_network.private.id
  static_route {
      destination_prefix = "0.0.0.0/0"
      next_hop_address   = yandex_compute_instance.node01-nginx.network_interface.0.ip_address
    }
}