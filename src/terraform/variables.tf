variable "YC_ACCESS_KEY" {
  //default = ""
  type    = string
}

variable "YC_SECRET_KEY" {
  //default = ""
  type    = string
}

variable "YC_CLOUD_ID" {
  //default = ""
  type      = string
}

variable "YC_FOLDER_ID" {
  //default = ""
  type    = string
}

variable "YC_ZONE" {
  default = "ru-central1-a"
}

variable "YC_ZONE2" {
  default = "ru-central1-b"
}

variable "YC_BUCKET_NAME" {
  default = "sa-netology-bucket"
}

variable "SUBNET1" {
  type = tuple([string])
  default = (["192.168.100.0/24"])
}

variable "SUBNET2" {
  type = tuple([string])
  default = (["192.168.101.0/24"])
}

variable "DOMAIN_NAME" {
  type = string  
  default = "domloro.xyz"
}

variable "REG_EMAIL" {
  type = string  
  default = "martandr2011@yandex.ru"
}

variable "IMAGE_ID" {
  description = "ubuntu-2004 image"
  default     = "fd81u2vhv3mc49l1ccbb"
}

variable "CORE_FRACTION" {
  type    = number
  default = 20
}

variable "PREEMPTIBLE" {
  type    = bool
  default = true
}


variable "TEST_CERT" {
//  default = "--test-cert" // TEST
  default = "" // PROD
}

variable "DB_REPLICATION_USER" {
  default = "replication"
}

variable "DB_REPLICATION_USER_PASSWORD" {
  default = "replusr123"
}
