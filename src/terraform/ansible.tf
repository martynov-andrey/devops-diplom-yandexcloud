//resource "null_resource" "clearknownhosts" {
//  provisioner "local-exec" {
//    command = "ssh-keygen -f ~/.ssh/known_hosts -R '${var.DOMAIN_NAME}'"
//  }
//} 

resource "null_resource" "wait" {
  provisioner "local-exec" {
    command = "sleep 100"
  }

  depends_on = [
    local_file.inventory
  ]
}

resource "null_resource" "certbot" {
  provisioner "local-exec" {
    command = "ANSIBLE_FORCE_COLOR=1 ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i ../ansible/inventory ../ansible/certbot.yml"
  }

  depends_on = [
    null_resource.wait
  ]
}

resource "null_resource" "nginx" {
  provisioner "local-exec" {
    command = "ANSIBLE_FORCE_COLOR=1 ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i ../ansible/inventory ../ansible/nginx.yml"
  }

  depends_on = [
    null_resource.certbot
  ]
}
 
resource "null_resource" "mysql" {
  provisioner "local-exec" {
    command = "ANSIBLE_FORCE_COLOR=1 ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i ../ansible/inventory ../ansible/mysql.yml"
  }

  depends_on = [
    null_resource.nginx
  ]
}

resource "null_resource" "wordpress" {
  provisioner "local-exec" {
    command = "ANSIBLE_FORCE_COLOR=1 ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i ../ansible/inventory ../ansible/wordpress.yml"
  }

  depends_on = [
    null_resource.mysql
  ]
}

resource "null_resource" "gitlab" {
  provisioner "local-exec" {
    command = "ANSIBLE_FORCE_COLOR=1 ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i ../ansible/inventory ../ansible/gitlab.yml"
  }

  depends_on = [
    null_resource.wordpress
  ]
}

resource "null_resource" "gitlab-runner" {
  provisioner "local-exec" {
    command = "ANSIBLE_FORCE_COLOR=1 ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i ../ansible/inventory ../ansible/gitlab_runner.yml"
  }

  depends_on = [
    null_resource.gitlab
  ]
}

resource "null_resource" "monitoring" {
  provisioner "local-exec" {
    command = "ANSIBLE_FORCE_COLOR=1 ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i ../ansible/inventory ../ansible/monitoring.yml"
  }

  depends_on = [
    null_resource.gitlab-runner
  ]
}
