resource "yandex_vpc_network" "private" {
  name = "my-private-net"
}

resource "yandex_vpc_subnet" "subnet1" {
  name           = "subnet-${var.YC_ZONE}"
  zone           = "${var.YC_ZONE}"
  network_id     = yandex_vpc_network.private.id
  v4_cidr_blocks = "${var.SUBNET1}"

  depends_on = [
    yandex_vpc_network.private
  ]
}

resource "yandex_vpc_subnet" "subnet2" {
  name           = "subnet-${var.YC_ZONE2}"
  zone           = "${var.YC_ZONE2}"
  network_id     = yandex_vpc_network.private.id
  v4_cidr_blocks = "${var.SUBNET2}"
  route_table_id = yandex_vpc_route_table.route-nat.id

  depends_on = [
    yandex_vpc_network.private
  ]
}
