resource "yandex_storage_bucket" "state" {
  access_key = "${var.YC_ACCESS_KEY}"
  secret_key = "${var.YC_SECRET_KEY}"
  bucket     = var.YC_BUCKET_NAME
}
