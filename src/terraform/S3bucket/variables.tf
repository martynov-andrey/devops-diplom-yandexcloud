variable "YC_ACCESS_KEY" {
  type = string
}

variable "YC_SECRET_KEY" {
  type = string
}

variable "YC_ZONE" {
  default = "ru-central1-a"
}

variable "YC_BUCKET_NAME" {
  default = "sa-netology-bucket"
}

variable "YC_CLOUD_ID" {
  default = ""
}

variable "YC_FOLDER_ID" {
  default = ""
}