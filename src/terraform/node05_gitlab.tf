resource "yandex_compute_instance" "node05-gitlab" {
  name     = "node05-gitlab"
  hostname = "gitlab.${var.DOMAIN_NAME}"
  zone     = "${var.YC_ZONE2}"
  resources {
    core_fraction = "${var.CORE_FRACTION}"
    cores         = 4
    memory        = 4
  }
  scheduling_policy {
    preemptible = "${var.PREEMPTIBLE}"
  }
  boot_disk {
    initialize_params {
      image_id = "${var.IMAGE_ID}"
      name     = "root-node05-gitlab"
      type     = "network-nvme"
      size     = "10"
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet2.id
    nat       = false
  }
  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }

  depends_on = [
    yandex_vpc_subnet.subnet2
  ]
}