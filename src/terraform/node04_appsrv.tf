resource "yandex_compute_instance" "node04-appsrv" {
  name     = "node04-appsrv"
  zone     = "${var.YC_ZONE2}"
  hostname = "app.${var.DOMAIN_NAME}"
  resources {
    core_fraction = "${var.CORE_FRACTION}"
    cores         = 4
    memory        = 4
  }
  scheduling_policy {
    preemptible = "${var.PREEMPTIBLE}"
  }
  boot_disk {
    initialize_params {
      image_id = "${var.IMAGE_ID}"
      name     = "root-node04-appsrv"
      type     = "network-nvme"
      size     = "10"
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet2.id
    nat       = false
  }
  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }

  depends_on = [
    yandex_vpc_subnet.subnet2
  ]
}
