resource "yandex_vpc_network" "public" {
  name = "my-public-net"
}

resource "yandex_dns_zone" "zone1" {
  name   = "my-public-zone"    
  zone   = "${var.DOMAIN_NAME}."
  public = true

  depends_on = [
    yandex_vpc_network.public
  ]
}

resource "yandex_dns_recordset" "dns_record1" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "@.${var.DOMAIN_NAME}."
  type    = "A"
  ttl     = 60
  data    = ["${yandex_compute_instance.node01-nginx.network_interface.0.nat_ip_address}"]

  depends_on = [
    yandex_dns_zone.zone1
  ]
}

resource "yandex_dns_recordset" "dns_record2" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "www.${var.DOMAIN_NAME}."
  type    = "A"
  ttl     = 60
  data    = ["${yandex_compute_instance.node01-nginx.network_interface.0.nat_ip_address}"]

  depends_on = [
    yandex_dns_zone.zone1
  ]
}

resource "yandex_dns_recordset" "dns_record3" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "gitlab.${var.DOMAIN_NAME}."
  type    = "A"
  ttl     = 60
  data    = ["${yandex_compute_instance.node01-nginx.network_interface.0.nat_ip_address}"]

  depends_on = [
    yandex_dns_zone.zone1
  ]
}

resource "yandex_dns_recordset" "dns_record4" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "grafana.${var.DOMAIN_NAME}."
  type    = "A"
  ttl     = 60
  data    = ["${yandex_compute_instance.node01-nginx.network_interface.0.nat_ip_address}"]

  depends_on = [
    yandex_dns_zone.zone1
  ]
}

resource "yandex_dns_recordset" "dns_record5" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "prometheus.${var.DOMAIN_NAME}."
  type    = "A"
  ttl     = 60
  data    = ["${yandex_compute_instance.node01-nginx.network_interface.0.nat_ip_address}"]

  depends_on = [
    yandex_dns_zone.zone1
  ]
}

resource "yandex_dns_recordset" "dns_record6" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "alertmanager.${var.DOMAIN_NAME}."
  type    = "A"
  ttl     = 60
  data    = ["${yandex_compute_instance.node01-nginx.network_interface.0.nat_ip_address}"]

  depends_on = [
    yandex_dns_zone.zone1
  ]
}
