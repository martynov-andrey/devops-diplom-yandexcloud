terraform {
  backend "s3" {
    endpoint   = "storage.yandexcloud.net"
    bucket     = "sa-netology-bucket"
    region     = "ru-central1-a"
    key        = "terraform-stage/terraform.tfstate"
    //access_key = "${var.YC_ACCESS_KEY}"
    //secret_key = "${var.YC_SECRET_KEY}"
    skip_region_validation      = true
    skip_credentials_validation = true
  }
}