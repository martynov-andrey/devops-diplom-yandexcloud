resource "yandex_compute_instance" "node01-nginx" {
  name     = "node01-nginx"
  zone     = "${var.YC_ZONE}"
  hostname = "${var.DOMAIN_NAME}"
  resources {
    core_fraction = "${var.CORE_FRACTION}"
    cores         = 2
    memory        = 2
  }
  scheduling_policy {
    preemptible = "${var.PREEMPTIBLE}"
  }
  boot_disk {
    initialize_params {
      image_id = "${var.IMAGE_ID}"
      name     = "root-node01-nginx"
      type     = "network-nvme"
      size     = "10"
    }
  }
  network_interface {
    subnet_id      = yandex_vpc_subnet.subnet1.id
    nat            = true
  }
  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }

  depends_on = [
    yandex_vpc_subnet.subnet1
  ]
}
