output "int_ip_address_node01_nginx" {
  value = yandex_compute_instance.node01-nginx.network_interface.0.ip_address
}

output "int_ip_address_node02_db01" {
  value = yandex_compute_instance.node02-db01.network_interface.0.ip_address
}

output "int_ip_address_node03_db02" {
  value = yandex_compute_instance.node03-db02.network_interface.0.ip_address
}

output "int_ip_address_node04_appsrv" {
  value = yandex_compute_instance.node04-appsrv.network_interface.0.ip_address
}

output "ext_ip_address_node01_nginx" {
  value = yandex_compute_instance.node01-nginx.network_interface.0.nat_ip_address
}

output "int_ip_address_node05_gitlab" {
  value = yandex_compute_instance.node05-gitlab.network_interface.0.ip_address
}

output "int_ip_address_node06_runner" {
  value = yandex_compute_instance.node06-runner.network_interface.0.ip_address
}

output "int_ip_address_node07_monitoring" {
  value = yandex_compute_instance.node07-monitoring.network_interface.0.ip_address
}